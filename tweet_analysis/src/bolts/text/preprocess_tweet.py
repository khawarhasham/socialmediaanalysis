from __future__ import absolute_import, print_function, unicode_literals

from streamparse.bolt import Bolt
import simplejson as json
import os
#os.environ['NLTK_DATA']="/Volumes/STAFF-SAAD/Users/khawar/nltk_data"
#import nltk
#from nltk.corpus import stopwords
#https://github.com/s/preprocessor
import preprocessor as p
from HTMLParser import HTMLParser
import string
import itertools
import re
import sys
import traceback

class PreprocessTweet(Bolt):
    outputs=["tweet_id", "tweet_text"]
    def initialize(self, conf, ctx):
        #remove following categories from tweet text
        p.set_options(p.OPT.HASHTAG, p.OPT.URL, p.OPT.EMOJI, p.OPT.MENTION, p.OPT.RESERVED, p.OPT.SMILEY)
        self.htmlparser = HTMLParser()

    '''
    def filter_text(self, tweet_txt):
        #remove non-ascii characters
        tweet_txt = filter(lambda x: x in string.printable, tweet_txt)
        stopwords = nltk.corpus.stopwords.words('english') + ['said', 'by', 'got', '.', '...', '..', 'rt', '&amp;', 'amp']
        #1 tokenize the text
        tokens = nltk.wordpunct_tokenize(tweet_txt)
        #2 remove stop words and punctuations
        filter_tokens = [w for w in tokens if w not in string.punctuation and w not in stopwords]
        #return a combined string of filtered text
        return " ".join(filter_tokens)
    '''
    def remove_link(self, tweet_txt):
        #remove URLs and @mentions from the text
        #tweet_txt = re.sub(r"(?:\@|https?\://)\S+", "", tweet_txt)
        return re.sub(r"(?:\@|https?\://)\S+", "", tweet_txt)

    def remove_html_entities(self, tweet_txt):
        #remove html entities i.e. &gt; < > etc.
        return self.htmlparser.unescape(tweet_txt)

    def standardize_words(self, tweet_txt):
        #convert helllllo to hello; yooooo to yo; happppppy to happy etc.
        tweet_text = ''.join(''.join(t)[:2] for _,t in itertools.groupby(tweet_txt))
        return tweet_text

    def preprocess_tweet(self, tweet_txt):
        #remove URL, RT(twitter reserved words), mentions, emojis, hashtags, smileys
        return p.clean(tweet_txt)

    def process(self, tup):
        tweet_id = tup.values[0]
        tweet = tup.values[1]
        #print (tweet)
        tweet_text = tweet.lower()
        tweet_text = tweet_text.encode('UTF-8')
        tweet_text = tweet_text.decode('unicode_escape').encode('ascii', 'ignore')
        tweet_text = self.preprocess_tweet(tweet_text)
        tweet_text = self.standardize_words(tweet_text)
        tweet_text = self.remove_html_entities(tweet_text)
        #remove stopwords from the tweet_text for furhter analysis i.e. frequent words
        #filtered_text = self.filter_text(tweet_text)
        filtered_text = tweet_text
        #self.log("Emitting preprocessed tweet")
        #after preprocessing, check if the text still has atleast 4 parts. making some valid sentences.
        #otherwise ignore this tweet
        if len(filtered_text.split()) > 4:
            self.emit([tweet_id, filtered_text])
        #if everything goes fine, declare successful processing
        #self.ack(tup)  #storm.py automatically call this if there is no errors/exceptions
