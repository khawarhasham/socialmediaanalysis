from __future__ import absolute_import, print_function, unicode_literals

from collections import Counter
from streamparse.bolt import Bolt
from mongoclient import MongoClient
import config

#evaluation libs
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer

from sumy.evaluation import rouge_1, rouge_2
#from sumy.evaluation import cosine_similarity
import sys
import os

'''
This bolt will evaluate the incoming summary
with the reference summary generated by the selected reference algo.
However, it is also possible that the reference summary was not yet generated.
in that case, we will not have evaluation.
'''
class EvaluationBolt(Bolt):
    #outputs = ['doc_id', 'algo', 'status']
    def initialize(self, conf, ctx):
        self.algos = config.eval_algos
        self.mongo = MongoClient()
        self.gold_algo = config.GOLD_ALGO
        self.log('eval Initializing done')

    def process(self, tup):
        tup = tup.values
        doc_id = tup[0]
        algo = tup[1]
        self.log("eval received tup: %s, %s" % (doc_id, algo))

        if algo == 'DONOTHING' or algo == self.gold_algo:
            pass
        elif algo in self.algos:
            #search for document having db_id and algo
            doc = self.mongo.find_summary({'id':doc_id, 'algo': algo})
            #print(doc)
            if doc:
                gold_summary = ''.join([al['summary'] for al in doc['algos'] if al['method']==self.gold_algo])
                eval_summary = ''.join([al['summary'] for al in doc['algos'] if al['method']==algo])
                #self.log(gold_summary)
                #self.log(eval_summary)
                if not gold_summary:
                    self.log('gold summary for %s is empty. can not evalaute %s' %(doc_id, algo))
                elif not eval_summary:
                    self.log('eval summary for %s is empty. can not evalaute %s' %(doc_id, algo))
                else:# not gold_summary and not eval_summary:
                    eval_parser = PlaintextParser.from_string(eval_summary, Tokenizer('english'))
                    gold_parser = PlaintextParser.from_string(gold_summary, Tokenizer('english'))

                    rouge_1_res = rouge_1(eval_parser.document.sentences, gold_parser.document.sentences)
                    rouge_2_res = rouge_2(eval_parser.document.sentences, gold_parser.document.sentences)
                    #cosine_res  = cosine_similarity(eval_summary, gold_summary)
                    res = self.mongo.find_update_summary_eval(doc_id, doc['parent_id'],
                                                {'algo': algo,
                                                'rouge_1':rouge_1_res,
                                                'rouge_2':rouge_2_res})
                    if res:
                        self.log('evaluation of %s added in db' % algo)
                    else:
                        self.log('evaluation could not be added %s' % algo)
            else:
                self.log('None doc for %s, %s' % (doc_id, algo))
