from __future__ import absolute_import, print_function, unicode_literals

from streamparse.bolt import Bolt
from mongoclient import MongoClient
import config
import sys

class SummaryWriterBolt(Bolt):
    outputs = ['doc_id', 'algo']
    def initialize(self, conf, ctx):
        self.mongo = MongoClient()
        print ("Initializing SummaryWriterBolt")

    def process(self, tup):
        tup = tup.values
        #print (tup)
        chunk_id = tup[0] #tweet ids
        chunk_size = tup[1] #combined tweets size for this chunk
        summary = tup[2] #generated summary or error msg
        method = tup[3] #algo name
        ptime = tup[4] #processing time

        #first insert the source info
        #doc = {"ids": ids, "combined_text": 'CURRENTLY EMPTY.'}
        #doc_id = self.mongo.find_insert(doc)
        #add the algo summary
        doc = {'method': method, 'summary': summary, "ptime": ptime, 'size':chunk_size}
        res = self.mongo.insert_summary(parent_id=chunk_id, doc=doc)
        if res:
            self.log("Summary by algo %s added" % method)
            self.log("emitting doc id %s" % res['_id'])
            #emit it for evaluation
            if method != config.GOLD_ALGO:
                self.emit([str(res['_id']), method])
        else:
            self.log("Emitting fake tuple")
            self.emit(['NOTHING', 'DONOTHING'])

        #self.ack(tup)
