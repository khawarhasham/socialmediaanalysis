from sumy.summarizers.kl import KLSummarizer as KSummarizer
from sumy.summarizers.lex_rank import LexRankSummarizer as LrSummarizer
from sumy.summarizers.lsa import LsaSummarizer
from sumy.summarizers.text_rank import TextRankSummarizer as TrSummarizer
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer

from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words
import config

LANGUAGE = "english"
stemmer = Stemmer(LANGUAGE)

summarizers = {
'lexrank': LrSummarizer(stemmer),
'texrank': TrSummarizer(stemmer),
'kl': KSummarizer(stemmer),
'lsa': LsaSummarizer(stemmer)
}

def init(algo):
    summarizer = summarizers[algo]
    summarizer.stop_words = get_stop_words(LANGUAGE)
    return summarizer

def test(basealgo, refalgo):
    sentences = """That imbalance will become a theme of the Article 50 negotiations. It suggests that the British will have to do most of the compromising. Mrs May must not waste the two-year timetable haggling over a few billion, when trade worth vastly more hangs in the balance. The EU can help by agreeing to discuss the post-Brexit settlement in parallel with the debate about money. Rolling the lot into one would increase the opportunities for trade-offs that benefit both sides. But there is a danger of hardliners in London and Brussels making
    compromise impossible. Some in the European Commission are too eager to make a cautionary tale of Britain's exit. And they overestimate Mrs May's ability to sell a hard deal at home. The British public is unprepared for the exit charge, which is not mentioned in the government's white paper on the talks.
    The pro-Brexit press, still giddy from its unexpected victory last summer, will focus both on the shockingly large total and also on the details (here's one: the average Eurocrat's pension is double Britain's average household income). It has flattered Mrs May with comparisons to Margaret Thatcher, who wrung a celebrated rebate out of the EU in 1984. A small band of Brexiteer MPs have a Trumpian desire to carry out not just a hard Brexit but an invigoratingly disruptive one. Mrs May's working majority in Parliament is only 16.'
    """
    sentences = sentences.encode('UTF-8')
    sentences = sentences.decode('unicode_escape').encode('ascii', 'ignore')
    base_summ = init(basealgo)
    ref_summ = init(refalgo)
    base_parser = PlaintextParser.from_string(sentences, Tokenizer(LANGUAGE))
    ref_parser = PlaintextParser.from_string(sentences, Tokenizer(LANGUAGE))
    summary = []
    for sentence in base_summ(base_parser.document, config.SUMMARY_SENTENCES):
       summary.append(sentence._text)
    base_summary = ''.join(summary)
    print base_summary
    summary = []
    for sentence in base_summ(base_parser.document, config.SUMMARY_SENTENCES):
       summary.append(sentence._text)
    print summary

if __name__ == '__main__':
    test('lsa', 'kl')
