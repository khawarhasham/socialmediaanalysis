from __future__ import absolute_import
from __future__ import division, print_function, unicode_literals
from streamparse.bolt import Bolt

from collections import OrderedDict
from mongoclient import MongoClient
import config
import sys
import traceback
import time

class ChunkBolt(Bolt):
    outputs=["chunk_id"]
    def initialize(self, conf, ctx):
        self.count = 0
        self.chunk_tweets = OrderedDict()
        self.mongo = MongoClient()
        self.chunk_id = None

    def increase_count(self):
        self.count += 1

    def reset_count(self):
        self.count = 0

    def process(self, tup):
        tweet_id = tup.values[0]
        tweet_text = tup.values[1]
        tweet_text = tweet_text.encode('UTF-8')
        tweet_text = tweet_text.decode('unicode_escape').encode('ascii', 'ignore')
        chunk_doc = {'tweet_id': tweet_id, 'preprocess_text': tweet_text}

        if self.chunk_id and self.count % config.TWEETS_CHUNK_SIZE == 0:
            self.log("Emitting a new chunk of tweets with chunk_id %s" % self.chunk_id)
            self.emit([str(self.chunk_id)])
            self.chunk_id = None #reset chunk_id
            self.chunk_tweets.clear()
            self.reset_count()

        if self.chunk_id is None:
            self.log("Start creating a new chunk")
            #start a new chunk
            resp = self.mongo.insert_tweets({'size':config.TWEETS_CHUNK_SIZE, 'chunk':[chunk_doc]})
            self.log(resp)
            if resp:
                self.chunk_id = resp.inserted_id
                self.chunk_tweets[tweet_id] = tweet_text
                self.increase_count()
        else:
            #remove duplicate tweet text in current num_tweets windows
            stored_tweets = self.chunk_tweets.values()
            #if incoming tweet is not already stored, add it, otherwise remove it
            if tweet_text not in stored_tweets:
                #add this new tweet_text to current chunk
                self.mongo.add_tweet_chunk(self.chunk_id, {'chunk':chunk_doc})
                self.chunk_tweets[tweet_id] = tweet_text
                self.increase_count()
            else:
                self.log("Repeated tweet")
