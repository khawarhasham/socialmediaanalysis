from __future__ import absolute_import
from __future__ import division, print_function, unicode_literals
from streamparse.bolt import Bolt

#from sumy.parsers.html import HtmlParser
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lex_rank import LexRankSummarizer as Summarizer
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words
from mongoclient import MongoClient

import config
import sys
import traceback
import time

class LexRankBolt(Bolt):
    outputs=["chunk_id", "chunk_size", "summary", "method", "ptime"]
    def initialize(self, conf, ctx):
        self.mongo = MongoClient()
        LANGUAGE = "english"
        stemmer = Stemmer(LANGUAGE)
        self.summarizer = Summarizer(stemmer)
        self.summarizer.stop_words = get_stop_words(LANGUAGE)
        self.method = 'lexrank'

    def summary(self):
        SUMMARY_SENTENCES = config.SUMMARY_SENTENCES #num of sentences in summary
        summary = []
        #combined_tweets = '. '.join(self.combined_tweets.values())
        #combined_tweets = combined_tweets.encode('UTF-8')
        #combined_tweets = combined_tweets.decode('unicode_escape').encode('ascii', 'ignore')
        #self.log(combined_tweets)
        parser = PlaintextParser.from_string(self.combined_tweets, Tokenizer('english'))
        for sentence in self.summarizer(parser.document, SUMMARY_SENTENCES):
            summary.append(sentence._text)
        text_summary = ''.join(summary)
        return text_summary

    def process(self, tup):
        chunk_id = tup.values[0]
        self.log("Summary for chunk %s" % chunk_id)
        chunk_obj = self.mongo.get_chunk(chunk_id)
        if chunk_obj:
            tweet_text = '. '.join([obj['preprocess_text'] for obj in chunk_obj['chunk']])
            tweet_text = tweet_text.decode('unicode_escape').encode('ascii', 'ignore')
            self.combined_tweets = tweet_text
        else:
            self.combined_tweets = None

        if not self.combined_tweets:
            return

        summaryT = ""
        processing_time = time.time()
        text_size = 0
        try:
            summaryT = self.summary()
            text_size = len(self.combined_tweets)
        except:
            summaryT = 'Summary generation Failed %s:%s' % (sys.exc_info()[0], sys.exc_info()[1])

        processing_time = time.time() - processing_time
        self.emit([chunk_id, text_size, summaryT, self.method, processing_time])
        self.combined_text = None
