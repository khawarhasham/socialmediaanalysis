import pymongo
from pymongo.collection import ReturnDocument
from bson.objectid import ObjectId
import config
import sys
import random

class MongoClient(object):
    def __init__(self):
        self.conn = pymongo.MongoClient(config.MONGO_URLS)
        self.db = self.conn.twitter

    def insert_tweets(self, doc):
        try:
            doc_res = self.db.tweets.insert_one(doc)
            #print(doc_res)
            return doc_res
        except:
            print("Some error during inserting in mongodb")
            return None

    def find_insert(self, doc):
        try:
            #print(self.db.tweets)
            res = self.db.tweets.find_one(
                    {'ids':{'$all':doc['ids']}},
                    {"_id":True} )
            if res:
                print ("Document found with id: %s" % res)
                return res['_id']
            else:
                res = self.insert_tweets(doc)
                return res.inserted_id
        except:
            print ('Error in find_insert', sys.exc_info())
            return None

    def insert_summary(self, parent_id, doc):
        try:
            #print (self.db.processed)
            parent_id = ObjectId(parent_id)
            res = self.db.processed.find_one_and_update(
                        {'parent_id':parent_id},
                        {'$set': {'parent_id': parent_id},
                        '$addToSet':{'algos': doc}
                        },
                        projection={'_id':True},
                        upsert=True,
                        return_document=ReturnDocument.AFTER)

            return res
        except:
            print ('Error in insert_summary', sys.exc_info())
            return False

    def add_tweet_chunk(self, chunk_id, new_chunk_doc):
        try:
            res = self.db.tweets.find_one_and_update(
                        {'_id': chunk_id},
                        {'$addToSet':new_chunk_doc},
                        projection={'_id': True},
                        upsert=True,
                        return_document=ReturnDocument.AFTER
                        )
            return res
        except:
            print ('Error in add_tweet_chunk', sys.exc_info())
            return False

    def get_chunk(self, chunk_id):
        try:
            res = self.db.tweets.find_one(
                        {'_id':ObjectId(chunk_id)},
                        {'_id':False, 'chunk.preprocess_text':True}
                        )
            #print res
            return res
        except:
            print ('Error in get_chunk', sys.exc_info())
            return False

    def find_update_summary_eval(self, docid, parent_id, evaldoc):
        try:
            print ('updating summary eval')
            upd = self.db.processed.find_one_and_update(
                        {'_id': ObjectId(docid)},
                        {
                        #'$set':{'parent_id': parent_id},
                        '$addToSet':{'sum_eval': evaldoc}
                        },
                        upsert=True,
                        return_document=ReturnDocument.AFTER)
            return upd
        except:
            print ('Error in find_update_summary_eval', sys.exc_info())
            return False

    def find_summary(self, doc):
        try:
            summary = self.db.processed.find_one(
                            {'_id': ObjectId(doc['id']),
                            'algos.method':{'$in':[doc['algo']]}
                            })
            return summary
        except:
            print ('Error in find_summary', sys.exc_info())
            return False

def test_combined():
    m = MongoClient()
    doc={"ids":sorted([1234, 666, 777]), "text": "Combined Text"}
    pid = m.find_insert(doc)
    methods=['lsa', 'tr', 'lr', 'kl']
    selected = random.sample(methods,1)
    print("Inserting summary")
    print (m.insert_summary(pid, {'method': selected, 'summary':'%s summary' % selected}))
    sumy = m.find_summary({'id':'58ad71579e79e91a16c4774a', 'algo':'texrank'})
    summary = ''.join([al['summary'] for al in sumy['algos'] if al['method']=='texrank'])
    print(m.find_update_summary_eval('58ad71579e79e91a16c4774a','58ad71578a380007461d44e1',
                                {'algo':'texrank',
                                'rouge_1':0.64823,
                                'rouge_2':0.65234}))

def test_chunks():
    m = MongoClient()
    chunks={'chunks':[{'tweet_id':123, 'preprocess_text': 'This is good.'},
    {'tweet_id':124, 'preprocess_text':' Federer played great.'}]}
    #res = m.insert_tweets(chunks)
    #chunk_id = res.inserted_id
    chunk_id = ObjectId('58b1c68c8a38003913af8fa8')
    print ("chunk doc id",chunk_id)
    chunk = m.get_chunk(chunk_id)
    tweet_chunk = '. '.join([obj['preprocess_text'] for obj in chunk['chunks']])
    print tweet_chunk

if __name__ == '__main__':
    test_chunks()
#all records with all algo values
#db.processed.find({'algos.method':{"$all": ["kl", "lsa", "lexrank", "texrank"]}}).count()
#topology ran for 1h10m
#stream stats
"""Counter(
{'stream_finished': 1487764708.604031,
'stream_start': 1487760649.336768,
'tweets': 13655,
'tweet_english': 7747,
'tweet_nolinks': 7605,
'tweet_noenglish': 5908,
'stream_total_time': 4059.267263174057,
'tweet_text': 1308,
'tweet_links': 142})
"""
