from elasticsearch import Elasticsearch

import json

class ESClient(object):
    def __init__(self, hosts):
        eshosts = hosts
        self.client = Elasticsearch(hosts=eshosts)
        print self.client

    def createIndex(self, index_name):
        if (not self.client.exists(index=index_name) ):
            self.client.create(index=index_name, ignore=[400, 404])

    def indexTweet(self, index_name, doc_type, doc_body):
        resp = self.client.index(index=index_name, doc_type=doc_type, body=doc_body)
        if ( 'ok' in resp and resp['ok']):
            print 'tweet indexed.'
            return True
        return False

    def update_document(self, index_name, doc_type, doc_body, doc_id):
        resp = self.client.update(index=index_name, doc_type=doc_type, id=doc_id, body=doc_body)
        print resp
        if ( resp['result'] == 'updated' or resp['result'] == 'created'):
            return True
        return False

    def countIndexDoc(self, index_name):
        count = self.client.count(index=index_name)
        print index_name, count['count']

    def deleteDocuments(self, index_name, doc_type, match):
        resp = self.client.delete_by_query(index=index_name, body=match)
        print resp
        return resp
        
    def getTermVector(self):
        pass

if __name__ == '__main__':
    es = ESClient(['localhost'])
    body = '{"tweet_id":1, "summary": "Hella yo ya"}'

    print json.loads(body)
    ubody = '{"doc": {"article": "full article text here"}, "detect_noop": false, "doc_as_upsert": true}'
    #es.indexTweet("twitter", "tweet_text", json.loads(body))
    es.update_document("twitter", "tweet_text", json.loads(ubody), "AVnm5ia9vYI43T0afQdt")
    es.countIndexDoc("twitter")
