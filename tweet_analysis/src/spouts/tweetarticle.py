from streamparse.spout import Spout
from newspaper import Article
from urlparse import urlparse

import json
import sys
import re

sys.path.insert(1, '/Users/khawar/dev_area/python_work/bigdata-analysis')
import config
from tweet_queue import TweetQueue

class TweetArticleSpout(Spout):
    outputs=["tweet_id", "article_link", "article_text"]
    def initialize(self, stormconf, context):
        print ('intializing TweetArticleSpout')
        #print (stormconf)
        #print (context)
        tweet_queue = TweetQueue()
        #self.tweet_consumer = tweet_queue.get_consumer(str('tweet_article'), str('tweetgrp'))
        self.tweet_consumer = tweet_queue.get_consumer(str(config.KAFKA_TOPIC), str(config.KAFKA_GROUP))
        #print ('Got Consumer')
        self.news_agencies = config.news_agencies
        #print all variables in config.py 
        self.log([item for item in dir(config) if not item.startswith("__")])

    def get_article_from_tweet(self, text):
        pattern = r'(https?://[^\s]+)'
        links = re.findall(pattern, text)
        return links

    def get_emd_article_tweet(self, tweet):
        links = {}

        for url in tweet['entities']['urls']:
            if url['expanded_url'] is None:
                continue
            expanded_url = url['expanded_url']
            parsed_uri = urlparse(expanded_url)
            #print parsed_uri.netloc, expanded_url
            twitter_pattern = r'^(https?:\/\/)?((w{3}\.)?)twitter\.com\/'
            if re.findall(twitter_pattern, expanded_url):
                self.log("Skipping twitter link %s" % expanded_url)
                continue
            else:
                self.log("Not a twitter link %s" % expanded_url)
            links[parsed_uri.netloc] = expanded_url

        found = False
        #TODO: do the filtering on all the urls
        for news_source, news_link in links.items():
            for n in self.news_agencies:
                if ( news_source.find(n)>=0):
                    found = True
                    break
        if found:
            return links.values()
        else:
            self.log("No links found")
            return None

    def next_tuple(self):
        tweet_counter = 0
        self.log(self.tweet_consumer)
        for message in self.tweet_consumer:
            self.log("TweetArticleSpout Received a new message")
            self.log (message.value)
            if message is not None:
                msg = json.loads(message.value)
                if ('start' in msg):
                    msg_start = msg['start']
                    tweet = msg['data']
                else:
                    tweet = msg
                #print message.value
                #tweet_text = tweet['text'] #TODO: ensure encodings
                #self.log("pasring " + tweet_text)

                #links = self.get_article_from_tweet(tweet_text)
                links = self.get_emd_article_tweet(tweet)
                if links:
                    self.log('TweetArticleSpout found articles= %s' % len(links))
                    link = links[0]
                    self.log(link)
                    #self.log("TweetArticleSpout Downlaod article: " + link)
                    link_article = Article(link, language='en')
                    try:
                        link_article.download()
                        link_article.parse()
                        self.log('article downloaded')
                        self.emit([tweet['id'], link, link_article.text.encode('UTF-8')])
                    except:
                        #if something happened during downloading or parsing
                        self.log("Error during link downloading/parsing: %s" % sys.exc_info()[0])
                        #generate a custome error
                        self.emit([tweet['id'], link, config.LINK_PARSE_ERROR])

                tweet_counter += 1
                #print 'Tweets emitted {}'.format(tweet_counter)
                self.log("TweetArticleSpout Tweets processed: %d" % tweet_counter)
            if tweet_counter % config.KAFKA_TWEETS_OFFSET == 0:
                self.tweet_consumer.commit_offsets()
                self.log("TweetArticleSpout Committing Offsets for kafka")
