from __future__ import absolute_import, print_function, unicode_literals

from streamparse.spout import Spout
import simplejson as json
import itertools
import config
import sys

#sys.path.insert(1, '/Users/khawar/dev_area/python_work/bigdata-analysis')
from tweet_queue import TweetQueue

class TweetSpout(Spout):
    outputs=["tweet_id", "tweet_text"]
    def initialize(self, stormconf, context):
        #print ('intializing TweetSpout')
        #print (stormconf)
        #print (context)
        tweet_queue = TweetQueue()
        self.tweet_consumer = tweet_queue.get_consumer(str('tweet'), str('tweetgrp'))
        #print ('Got Consumer')

    def next_tuple(self):
        tweet_counter = 0
        for message in self.tweet_consumer:
            #self.log("Received a new message")
            if message is not None:
                #self.log(type(message.value))
                tweet = json.loads(message.value)
                #print message.offset, tweet['user'], tweet['text'], tweet['timestamp_ms'], tweet['user']['utc-offset']
                #tweet_text = unicode(tweet['text'])
                tweet_text = tweet['text']
                tweet_text = tweet_text.encode('UTF-8')
                #self.log(tweet_text)
                self.emit([tweet['id'], tweet_text])
                tweet_counter += 1

            if tweet_counter % config.KAFKA_TWEETS_OFFSET == 0:
                self.tweet_consumer.commit_offsets()
                self.log("Tweets processed: %d" % tweet_counter)
                #self.log("TweetSpout Committing Offsets for kafka")
