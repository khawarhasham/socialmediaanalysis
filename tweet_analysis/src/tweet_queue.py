from pykafka.cluster import Cluster
from pykafka.topic import Topic
from pykafka import KafkaClient
import logging
import simplejson as json
from urlparse import urlparse
import signal
import sys
import re
import config

class TweetQueue(object):
	def __init__(self):
		print 'Initiating Tweet Queue'
		#get Kafka client
		#hosts = "127.0.0.1:9092"
		hosts = config.KAFKA_HOST
		self.client = KafkaClient(hosts = hosts)

	def show_kafka(self):
		print self.client
		print self.client.cluster
		print 'topics: {}'.format(self.client.cluster.topics)
		print 'brokers: {}'.format(self.client.cluster.brokers)

	def produce_msg(self, topic, tweet):
		#with topic_handler.get_sync_producer() as producer:
		with self.get_sync_producer(topic) as producer:
			print 'Publishing a new tweet message for topic:{}'.format(topic)
			producer.produce(tweet)

	def get_sync_producer(self, topic):
		topic_handler = None
		if topic in self.client.topics:
			topic_handler = self.client.topics[topic]
		else:
			print '{} not found'.format(topic)
			return None
		#sync producer
		#return topic_handler.get_sync_producer()
		#for async_producer. This gives better throughput
		return topic_handler.get_producer()

	def get_article_from_tweet(self, text):
		pattern = r'(https?://[^\s]+)'
		links = re.findall(pattern, text)
		return links

	def consume_msg(self, topic):
		print 'Listening to {} for consumption.'.format(topic)
		#consumer = self.client.topics[topic].get_simple_consumer(consumer_group="tweetgrp")
		consumer = self.get_consumer(topic, "tweetgrp")

		print consumer
		tweet_counter = 0
		for message in consumer:
			print message.value, type(message.value)
			consumer.commit_offsets()
			continue
			if message is not None:
				print message.value, type(message.value)
				#tweet = json.loads(message.value)
				tweet = message.value
				print tweet['text']
				if 'media' in tweet['entities']:
					print 'media type:', tweet['entities']['media'][0]['type']
				else:
					print tweet['entities']
				for url in tweet['entities']['urls']:
					if url['expanded_url'] is None:
						continue
					expanded_url = url['expanded_url']
					parsed_uri = urlparse(expanded_url)
					print parsed_uri.netloc, expanded_url

				print self.get_article_from_tweet(tweet['text'])
				#tweet = json.loads(message.value)
				#print tweet
				#print message.offset, message.value, tweet['user']
				#print message.offset, tweet['user']['id'], tweet['user']['screen_name'], \
				#tweet['user']['location'], tweet['text'], tweet['timestamp_ms'], \
				#tweet['user']['time_zone'],tweet['user']['utc_offset']

				tweet_counter += 1
				#consumer.commit_offsets()
			if tweet_counter % 500 == 0:
				print 'committing current offsets'
				consumer.commit_offsets()

	def get_consumer(self, topic, consumer_grp):
		print 'retrieving consumer for topic {}'.format(topic)
		consumer = self.client.topics[topic].get_simple_consumer(consumer_group=consumer_grp)
		return consumer

	def disconnect(self):
		print 'Closing connection gracefully.'
		print '\tThis is currently not supported. At least, I could not find it anywhere on Github'
		#client.cluster.disconnect()

def handler(signum, frame):
	print 'Got interrupt {}'.format(signum)
	sys.exit(1)

def usage():
	print 'Usage: {} [c|p] <topic> \n c means consumer \n p means producer'.format(sys.argv[0])

if __name__ == '__main__':
	signal.signal(signal.SIGINT, handler)
	if len(sys.argv) < 2:
		usage()
		sys.exit(1)

	tq = TweetQueue()
	tq.show_kafka()
	if sys.argv[1] == 'c':
		topic = sys.argv[2]
		tq.consume_msg(topic)
	elif sys.argv[1] == 'p':
		topic = sys.argv[2]
		tq.produce_msg(topic, '{"Test": "Tweet is this", "ID": "Khawar"}')
	tq.disconnect()
