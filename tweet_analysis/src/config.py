#elasticsearch config
ES_HOSTS=["localhost"]
ES_INDEX="twitter"
ES_DOCTYPE="tweet_article"

#mongodb
MONGO_URLS=["mongodb://localhost"]

#Apache Kafka config
KAFKA_HOST="127.0.0.1:9092"
KAFKA_TOPIC="tweet_article"
KAFKA_GROUP="tweetgrp"
KAFKA_TWEETS_OFFSET=5

#tweet process type
#this will control the type of events tweetstream publishes to kafka
TWEET_ARTICLE=False
TWEET_TEXT=True
SUMMARY_TWEETS = 15
TWEETS_CHUNK_SIZE = 15
#ERRORS
LINK_PARSE_ERROR="KHAWAR: LINK DP ERROR"

#Parsing news sites
news_agencies = ['businessinsider', 'telegraph', 'cnn', 'bbc', 'independent', 'washington', 'nyt', 'guardian']

#evaluation
eval_algos = ['lsa', 'texrank', 'kl', 'lexrank', 'luhn', 'edmund']
GOLD_ALGO = 'tfidf'
SUMMARY_SENTENCES = 5
