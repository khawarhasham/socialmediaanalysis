from streamparse import Grouping, Topology
#spouts
from spouts.tweetext import TweetSpout
from spouts.tweetarticle import TweetArticleSpout
#bolts
from bolts.text.preprocess_tweet import PreprocessTweet
from bolts.text.sumylr import LexRankBolt
from bolts.text.sumylsa import LsaBolt
from bolts.text.sumytr import TextRankBolt
from bolts.text.sumykl import KLBolt
from bolts.text.sumyluhn import LuhnBolt
from bolts.text.sumyedmund import EdmundsonBolt


from bolts.text.goldsum import GoldBolt
from bolts.text.chunk import ChunkBolt


from bolts.text.summarywriter import SummaryWriterBolt
from bolts.text.eval import EvaluationBolt

class TwitterTopology(Topology):
    #set topology-wise configuration
    #for local,
    #config = {'topology.configuration': {'PYTHONPATH':'/Volumes/STAFF-SAAD/Users/khawar/anaconda/envs/bigdata/lib/python2.7/:$PYTHONPATH'}}

    #topology part for tweet text analysis
    tweetSpout = TweetSpout.spec(name='tweet-spout')
    preprocess = PreprocessTweet.spec(name='tweet_preprocess',
                        inputs={tweetSpout: Grouping.SHUFFLE},
                        par=1)

    #create chunk_tweets
    chunkBolt = ChunkBolt.spec(name='chunk-bolt',
                        inputs={preprocess: Grouping.SHUFFLE})
    #gold standard bolt
    goldBolt = GoldBolt.spec(name='gold-algo',
                        inputs={chunkBolt: Grouping.SHUFFLE})

    lexRankBolt = LexRankBolt.spec(name='lex-algo',
                        inputs={goldBolt: Grouping.fields("chunk_id")})
    lsaBolt = LsaBolt.spec(name='lsa-algo',
                        inputs={goldBolt: Grouping.fields("chunk_id")})
    texRankBolt = TextRankBolt.spec(name='tex-algo',
                        inputs={goldBolt: Grouping.fields("chunk_id")})
    kLBolt = KLBolt.spec(name='kl-algo',
                        inputs={goldBolt: Grouping.fields("chunk_id")})
    luhnBolt = LuhnBolt.spec(name='luhn-algo',
                        inputs={goldBolt: Grouping.fields("chunk_id")})
    edmundBolt = EdmundsonBolt.spec(name='edmund-algo',
                        inputs={goldBolt: Grouping.fields("chunk_id")})

    summaryWriterBolt = SummaryWriterBolt.spec(name='mongo-writter',
                        inputs={goldBolt: Grouping.SHUFFLE,

                                lexRankBolt: Grouping.SHUFFLE,
                                lsaBolt: Grouping.SHUFFLE,
                                texRankBolt: Grouping.SHUFFLE,
                                kLBolt: Grouping.SHUFFLE,
                                luhnBolt: Grouping.SHUFFLE,
                                edmundBolt: Grouping.SHUFFLE}
                        )
    """
    summaryWriterBolt2 = SummaryWriterBolt.spec(name='lr-writter',
                        inputs={lexRankBolt: Grouping.SHUFFLE}
                        )
    """

    evaluationBolt = EvaluationBolt.spec(name='eval-summ',
                        inputs={summaryWriterBolt: Grouping.SHUFFLE}
                        )
