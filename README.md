# Instructions
This project code consists of two main parts. 
First part is to access twitter public stream and the second part is to process the incoming tweets using Apache Storm topology.

## Accessing Twitter Stream
The code to access twitter stream is placed in tweetstream.py. It internally uses tweet_queue.py file to interact with Apache Kafka. The connection configurations are loaded from sconfig.py file. All these files are placed in the current directoy. To run the twitter stream, use following command.

```
python tweetstream stream <keywords>
```
where `keywords` can be any string e.g. health.

Once you kill this process or it exists due to exception such as twitter limit etc., you will see the stats displayed. An example is given below.

```
Counter(
{'stream_finished': 1487764708.604031,
'stream_start': 1487760649.336768,
'tweets': 13655,
'tweet_english': 7747,
'tweet_nolinks': 7605,
'tweet_noenglish': 5908,
'stream_total_time': 4059.267263174057,
'tweet_text': 1308,
'tweet_links': 142})
```

### Requirements
* Apache Kafka Server and pykafka library
	* used as queue to store incoming tweets before processing them. The code implements a PUB/SUB model using Kafka.
* tweepy; a python library for twitter 

## Stream Processing using Apache Storm
The apache storm topology and its related code is placed inside the **tweet\_analysis** folder. The topology is implemented in python using the streamparse library. The visual image of the current topology is placed at **tweet_analysis/twitter.png**. According to the storm cluster settings, put the configuration parameter values in config.json file, which is used during the execution and submission of the topology. Copy the **_config.json** to **config.json** file. Once everything is correctly set, run the following command.

```
sparse submit -n twitter
```
This command is used to submit the topology, having name twitter. The command will look for twitter.py file inside the **topology** directory and builds the topology jar. The required library dependencies are loaded from virtualend/twitter.txt file and installed on the path configured in the config.json file.
You should see your topology in Storm UI if there is no error found on this step.

**Note:** Since this command is used to submit topology on the storm cluster, make sure that the nimbus, supervisor and other storm-related required services are running and storm cluster is up.

### Requirements
* Apache Storm cluster
* streamparse library
* Running Apache Kafka Server and its connection URL in src/config.py file
* MongoDB server
* Required python dependencies are already provided in twitter.txt. They will be installed by sparse.
* MongoDB connection parameter used in the code should be configured in **tweet_analysis/src/config.py** file.


#### Other Tips
To increase the heartbeat or timeout frequency, put the following lines in storm.yaml file

```
nimbus.task.timeout.secs: 600
nimbus.supervisor.timeout.secs: 600
supervisor.worker.timeout.secs: 600
nimbus.monitor.freq.secs: 5
```

# Generate Results
In order to generate two graphs i.e. avg processing time by algorithms and avg rouge scores, a new component `results/eval.py` is added. Execute it as follows

```
python result/eval.py <chunk size>
```
where chunk size is the number of tweets in one work unit. A Work unit means the aggregation of tweets text to generate input text for summarizers.

## Dependencies
following python libraries are required.

+ pymongo
+ pandas
+ matlibplot