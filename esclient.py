from elasticsearch import Elasticsearch
import json

class ESClient(object):
    def __init__(self, hosts):
        eshosts = hosts
        self.client = Elasticsearch(hosts=eshosts)
        print self.client

    def createIndex(self, index_name):
        if (not self.client.exists(index=index_name) ):
            self.client.create(index=index_name, ignore=[400, 404])

    def indexTweet(self, index_name, doc_type, doc_body):
        resp = self.client.index(index=index_name, doc_type=doc_type, body=doc_body)
        if ( 'ok' in resp and resp['ok']):
            print 'tweet indexed.'
            return True
        return False

    def update_document(self, index_name, doc_type, doc_body, doc_id):
        resp = self.client.update(index=index_name, doc_type=doc_type, id=doc_id, body=doc_body)
        print resp
        if ( resp['result'] == 'updated' or resp['result'] == 'created'):
            return True
        return False

    def countIndexDoc(self, index_name):
        count = self.client.count(index=index_name)
        print index_name, count['count']

    def deleteDocuments(self, index_name, doc_type, match):
        resp = self.client.delete_by_query(index=index_name, body=match)
        print resp

    def getTermVector(self):
        pass

def index_document(es, tupVals):
    tweet_id = tupVals[0]
    link = tupVals[1]
    link_text = tupVals[2]
    score = json.loads(tupVals[3])
    doc_body = {'doc': {'tweet_id': tweet_id, 'link':link,
                'article_text': link_text,
                'article_score': score}, 'doc_as_upsert': True }
    resp = es.update_document('twitter', 'twitter_article', doc_body, tweet_id)
    print("esloader document update:%s" % resp)
    return resp

if __name__ == '__main__':
    es = ESClient(['localhost'])
    body = '{"tweet_id":1, "summary": "Hella yo ya"}'

    print json.loads(body)
    ubody = '{"doc": {"article": "full article text here"}, "detect_noop": false, "doc_as_upsert": true}'
    tup = (833953611596046336, u'https://www.theguardian.com/politics/2017/feb/20/mps-pour-scorn-on-racist-and-sexist-donald-trump-in-state-visit-debate?CMP=share_btn_tw', u'British MPs lined up on Monday to pour scorn on a \u201cracist and sexist\u201d Donald Trump, who they said should not be allowed to come to Britain for a state visit because of the risk it would embarrass the Queen.  The US president was compared to a \u201cpetulant child\u201d and had his intelligence questioned by MPs during a three-hour debate triggered after more than 1.8m people signed a petition urging Theresa May to cancel her invitation.  So many politicians packed into Westminster Hall for the debate that they had to have their speeches limited to five minutes each.  Alex Salmond said he was unsure over whether to be appalled by the morality of the invitation or astonished by its stupidity.  \u201cAs an example of fawning subservience, the prime minister holding hands [with Trump] would be difficult to match,\u201d the former Scottish first minister said. \u201cTo do it in the name of shared values was stomach churning. What exactly are the shared values that this house, this country would hope to have?\u201d  Labour\u2019s Paul Flynn said that only two US presidents had been accorded a state visit to Britain in more than half a century and it was \u201ccompletely unprecedented\u201d that Trump had been issued his within seven days of his presidency.    Flynn \u2013 who started the debate because he is on the petitions committee \u2013 said Trump would hardly be silenced by the invitation being rescinded, accusing him of a \u201cceaseless incontinence of free speech\u201d.  Asked by Caroline Lucas, co-leader of the Green party, if Trump\u2019s views on climate science should also be taken into account, Flynn responded that the president had shown \u201ccavernous depths of scientific ignorance\u201d on the issue.  They were speaking as thousands of demonstrators descended on Parliament Square to protest against the visit, chanting and waving placards reading \u201cno to racism; no to Trump\u201d. The shadow home secretary, Diane Abbott, addressed the crowds, as did Lucas \u2013 who emerged from the debate to describe Trump as a \u201cbully and a bigot\u201d.  Inside the chamber, Flynn was criticised by a Conservative MP when he quoted the Observer\u2019s Andrew Rawnsley, who has described the visit as the government \u201cpimping out the Queen for Donald Trump\u201d.  Jacob Rees-Mogg responded that it was out of order \u201cto refer to pimping out our sovereign\u201d and argued that no one had complained when Emperor Hirohito came on a state visit to the UK, who he said was responsible for \u201cthe rape of Nanking\u201d.  Rees-Mogg was one of a number of Tories to defend both the president and May for issuing a state visit. Nigel Evans, MP for Ribble Valley, warned against sneering at the 61 million Americans who voted for the president, describing them as \u201cthe forgotten people\u201d.  Adam Holloway, Conservative MP for Gravesham, said that while Trump\u2019s travel ban on people from seven Muslim-majority countries was absurd, it was \u201crather refreshing\u201d to see a politician actually do what they had promised. Crispin Blunt, who chairs the foreign affairs committee, said the Queen would be embarrassed if the invitation was now withdrawn.  Edward Leigh, Conservative MP for Gainsborough, told colleagues that he was going to make a \u201cdifficult argument\u201d and then claimed that Trump\u2019s racism and misogyny had been overstated. \u201cWhich one of us has not made some ridiculous sexual comment at some point in his past,\u201d he said, prompting an angry response from female MPs.  A number of female MPs stood up to complain of Trump\u2019s sexism, with Paula Sherriff, Labour MP for Dewsbury and Mirfield, quoting his infamous \u201cgrab her by the pussy\u201d comment, which she said was sexual assault.  Rushanara Ali, Labour MP for Bethnal Green and Bow, urged the prime minister to remove her invitation because of the \u201cdamage to the Queen\u201d of the association. Naz Shah, Labour MP for Bradford West, said she had once urged Trump to come to her constituency to share a curry and meet a Muslim chief superintendent, headteacher, health workers and so on.  \u201cBut to do so now that he is president will only reinforce his actions, his divisive racist and sexist messages. This flies in the face of everything we stand for. We cannot support what he is doing,\u201d she said.    David Lammy, the Labour MP for Tottenham, warned that African Americans were afraid of the presidency, saying Trump was supported by the Ku Klux Klan and had white supremacists in his inner circle.  Salmond also said that Trump would not have forgotten previous insults of him by the prime minister, and said that his experience of dealing with the former reality television star, who owns golf courses in Scotland, showed May was in a difficult position. \u201cFrom my experience negotiating with Donald Trump, never ever do it from a weak position because the result will be a total disaster.\u201d  However those close to ministers made clear that there would be no rowing back on the invitation to Trump and hit out at MPs. \u201cIt would be interesting to see how much time and public money was wasted on today\u2019s debate - it achieved nothing apart from offering some, who have nothing better to do, the opportunity to grandstand. The British people will be more interested in the important debate in the Lords on Brexit,\u201d said a government source.  ',
    u'{"sentimentscore": -0.37442523426417695, "sentiment": "neg"}')
    #print tup
    #index_document(es, tup)
    es.countIndexDoc("twitter")
    docs={'query': {'exists': {'field':"link"}}}
    es.deleteDocuments('twitter', 'twitter_article', docs)
    #es.indexTweet("twitter", "tweet_text", json.loads(body))
    #es.update_document("twitter", "tweet_text", json.loads(ubody), "AVnm5ia9vYI43T0afQdt")
    es.countIndexDoc("twitter")
