import pandas as pd
import matplotlib
#to avoid x-server backend issue
#suggestion from http://stackoverflow.com/questions/2801882/generating-a-png-with-matplotlib-when-display-is-undefined
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
params = {'xtick.labelsize':'12',
         'ytick.labelsize':'12'}
pylab.rcParams.update(params)

def plot_combine_line(df, fname, fig_meta):
    print df
    fig = plt.figure()
    #fig, ax = plt.subplots()
    df.groupby(["algo"]).plot(kind='line', x='chunk', y='avg_time', alpha=0.5, rot=0)
    plt.suptitle(fig_meta['title'], fontsize=12)
    plt.xlabel(fig_meta['x-axis'], fontsize=14)
    plt.ylabel(fig_meta['y-axis'], fontsize=14)
    #plt.set_xticklabels(df.index,rotation=90)
    plt.savefig(fname, bbox_inches='tight')

def plot_2(df, fname, fig_meta):
    #http://matplotlib.org/api/markers_api.html#module-matplotlib.markers
    styles = {'kl': 'bs-', 'edmund':'ro-',
              'lsa': 'y^-','luhn':'*-', 'lexrank':'p-',
              'textrank':'x-', 'tfidf':'D-'}
    fig, ax = plt.subplots()
    labels = []
    for key, grp in df.groupby(['algo']):
        print 'ploting key ', key
        print grp
        ax = grp.sort(['chunk']).plot(xticks=[8, 10,15,20,25,30, 32], style=styles[key], ax=ax, kind='line', x='chunk', y='avg_time')
        labels.append(key)
    lines, _ = ax.get_legend_handles_labels()
    ax.grid(True)
    ax.legend(lines, labels, loc='best')
    #plt.suptitle(fig_meta['title'], fontsize=12)
    plt.xlabel(fig_meta['x-axis'], fontsize=14)
    plt.ylabel(fig_meta['y-axis'], fontsize=14)
    plt.savefig(fname, bbox_inches='tight')

ds = [{u'avg_size': 2755.0377358490564, u'_id': {u'algo': u'kl', u'size': 30}, u'avg_time': 0.19288958243603976}, {u'avg_size': 2757.4814814814813, u'_id': {u'algo': u'lexrank', u'size': 30}, u'avg_time': 0.06066393852233887}, {u'avg_size': 2757.4814814814813, u'_id': {u'algo': u'texrank', u'size': 30}, u'avg_time': 0.043196519215901695}, {u'avg_size': 2757.4814814814813, u'_id': {u'algo': u'luhn', u'size': 30}, u'avg_time': 0.048756175571017794}, {u'avg_size': 2757.4814814814813, u'_id': {u'algo': u'tfidf', u'size': 30}, u'avg_time': 0.019377770247282804}, {u'avg_size': 2277.725490196078, u'_id': {u'algo': u'kl', u'size': 25}, u'avg_time': 0.12518717260921702}, {u'avg_size': 2278.9423076923076, u'_id': {u'algo': u'edmund', u'size': 25}, u'avg_time': 0.05518858707868136}, {u'avg_size': 2278.9423076923076, u'_id': {u'algo': u'texrank', u'size': 25}, u'avg_time': 0.03456703516153189}, {u'avg_size': 2757.4814814814813, u'_id': {u'algo': u'lsa', u'size': 30}, u'avg_time': 0.0690622329711914}, {u'avg_size': 2278.9423076923076, u'_id': {u'algo': u'lexrank', u'size': 25}, u'avg_time': 0.04958231632526104}, {u'avg_size': 911.5, u'_id': {u'algo': u'texrank', u'size': 10}, u'avg_time': 0.013553595542907715}, {u'avg_size': 1326.1333333333334, u'_id': {u'algo': u'lexrank', u'size': 15}, u'avg_time': 0.03492613236109416}, {u'avg_size': 1326.1333333333334, u'_id': {u'algo': u'tfidf', u'size': 15}, u'avg_time': 0.016457676887512207}, {u'avg_size': 911.5, u'_id': {u'algo': u'tfidf', u'size': 10}, u'avg_time': 0.0075683116912841795}, {u'avg_size': 1326.1333333333334, u'_id': {u'algo': u'edmund', u'size': 15}, u'avg_time': 0.039269757270812986}, {u'avg_size': 1326.1333333333334, u'_id': {u'algo': u'kl', u'size': 15}, u'avg_time': 0.04087576468785604}, {u'avg_size': 911.5, u'_id': {u'algo': u'lsa', u'size': 10}, u'avg_time': 0.020058596134185792}, {u'avg_size': 911.5, u'_id': {u'algo': u'luhn', u'size': 10}, u'avg_time': 0.018737447261810303}, {u'avg_size': 1326.1333333333334, u'_id': {u'algo': u'texrank', u'size': 15}, u'avg_time': 0.029822444915771483}, {u'avg_size': 1326.1333333333334, u'_id': {u'algo': u'lsa', u'size': 15}, u'avg_time': 0.04135369062423706}, {u'avg_size': 1326.1333333333334, u'_id': {u'algo': u'luhn', u'size': 15}, u'avg_time': 0.03280152479807536}, {u'avg_size': 2278.9423076923076, u'_id': {u'algo': u'luhn', u'size': 25}, u'avg_time': 0.039411384325761065}, {u'avg_size': 911.5, u'_id': {u'algo': u'kl', u'size': 10}, u'avg_time': 0.017713272571563722}, {u'avg_size': 911.5, u'_id': {u'algo': u'lexrank', u'size': 10}, u'avg_time': 0.017367064952850342}, {u'avg_size': 1997.7936507936508, u'_id': {u'algo': u'lsa', u'size': 20}, u'avg_time': 0.04686522105383494}, {u'avg_size': 1997.7936507936508, u'_id': {u'algo': u'luhn', u'size': 20}, u'avg_time': 0.03718762549142989}, {u'avg_size': 911.5, u'_id': {u'algo': u'edmund', u'size': 10}, u'avg_time': 0.02462022304534912}, {u'avg_size': 2757.4814814814813, u'_id': {u'algo': u'edmund', u'size': 30}, u'avg_time': 0.06339332792494032}, {u'avg_size': 1997.7936507936508, u'_id': {u'algo': u'tfidf', u'size': 20}, u'avg_time': 0.014679352442423502}, {u'avg_size': 2278.9423076923076, u'_id': {u'algo': u'lsa', u'size': 25}, u'avg_time': 0.05774522744692289}, {u'avg_size': 1997.7936507936508, u'_id': {u'algo': u'lexrank', u'size': 20}, u'avg_time': 0.04063532087537977}, {u'avg_size': 1997.7936507936508, u'_id': {u'algo': u'texrank', u'size': 20}, u'avg_time': 0.0316088805122981}, {u'avg_size': 1997.7936507936508, u'_id': {u'algo': u'edmund', u'size': 20}, u'avg_time': 0.048346841146075535}, {u'avg_size': 1997.7936507936508, u'_id': {u'algo': u'kl', u'size': 20}, u'avg_time': 0.08276043240986174}, {u'avg_size': 2278.9423076923076, u'_id': {u'algo': u'tfidf', u'size': 25}, u'avg_time': 0.01616148306773259}]
resl = []
for doc in ds:
    did = doc['_id']
    algo = did['algo']
    chunk = did['size']

    d = {'chunk': chunk, 'algo': algo, 'avg_size': doc['avg_size'], 'avg_time': doc['avg_time']}
    #dd = pd.DataFrame.from_dict(d, "index").reset_index()
    resl.append(d)
#print resl
df = pd.DataFrame(resl)
#df = df.set_index(['chunk'])
#df.sort(['chunk'], ascending=True)
df.algo.replace(to_replace=dict(texrank='textrank'), inplace=True)
print df
#df.sort_index(inplace=True)
fig_meta={#'title':'Average Processing time by different summarizers \n on avg. text length=%s'%math.ceil(df.iloc[0][df.columns[0]]),
          'title':'',
          'x-axis': 'Tweet Chunk Sizes',
          'y-axis': 'Avg. Processing Time (sec)',
          }
#plot_combine_line(df, "ttcombined_algo_avg_ptime.pdf", fig_meta)
plot_2(df, "combined_algo_avg_ptime.pdf", fig_meta)
