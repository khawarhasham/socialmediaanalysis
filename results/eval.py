#plotting
#from pandas import DataFrame
import pandas as pd
import matplotlib
#from matplotlib import style
#print style.available
#style.use('ggplot')
#to avoid x-server backend issue
#suggestion from http://stackoverflow.com/questions/2801882/generating-a-png-with-matplotlib-when-display-is-undefined
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.axes3d import Axes3D
from matplotlib.mlab import griddata
from matplotlib import cm
import matplotlib.pylab as pylab
params = {'xtick.labelsize':'12',
         'ytick.labelsize':'12'}
pylab.rcParams.update(params)

import numpy as np
#mongodb
from pymongo import MongoClient
#others
from pprint import pprint
import math
import sys
from collections import defaultdict

MONGO_URLS=["mongodb://localhost"]
mongo = None
db = None
DRAW = True

def get_multi_chunk_algo_3d(chunk_sizes, summary_records, ttype):
    print 'chunk sizes', chunk_sizes
    print 'limit', summary_records
    ALGO_ENTRIES=7
    EVAL_ENTRIES=6
    agg_pipeline = [
                {"$lookup": {"from": 'tweets', 'localField': 'parent_id', 'foreignField': '_id', 'as': 'tweet_docs'}},
                {"$unwind": "$tweet_docs"},
                {"$match": {'tweet_docs.size':{"$in":chunk_sizes}}},
                {"$project":{'algos':True, 'sum_eval':{"$ifNull":["$sum_eval",[]]}, 'tweet_docs':True}},
                {"$project":{'algos.method':True, 'algos.size':True, 'algos.ptime':True, 'sum_eval':True,
                            "tca": {"$size": '$sum_eval'}, "ss": {"$size": '$tweet_docs.chunk'},
                            "ca":{"$size": '$algos'}}},
                {"$match":{"ss":{"$in": chunk_sizes}, "tca":EVAL_ENTRIES, "ca":ALGO_ENTRIES}},
                {"$unwind":"$algos"},
                {"$unwind":"$sum_eval"},
                {"$project": {"algos":1, "sum_eval":1, "ss":1, "comp":{"$cmp":["$algos.method","$sum_eval.algo"]}}},
                {"$match":{"comp":0}},
                {"$group":{"_id":{"size":"$ss","algo":"$algos.method"},
                    "avg_time":{"$avg":"$algos.ptime"},
                    "avg_size":{"$avg":"$algos.size"},
                    "rouge1":{"$avg":"$sum_eval.rouge_1"},
                    "rouge2":{"$avg":"$sum_eval.rouge_2"}}}
                ]
    res = db.processed.aggregate(agg_pipeline)
    res = list(res)
    #pprint (res)
    #resl = defaultdict(list)
    resl = []
    for doc in res:
        did = doc['_id']
        algo = did['algo']
        if algo == 'texrank':
            algo = 'textrank'
        chunk = did['size']
        d = {'chunk': chunk, 'algo': algo, 'avg_size': doc['avg_size'],
            'avg_time': doc['avg_time'], 'rouge1': doc['rouge1'], 'rouge2': doc['rouge2']}
        resl.append(d)
    #print resl
    df = pd.DataFrame(resl)
    df.algo.replace(to_replace=dict(texrank='textrank'), inplace=True)
    #print df
    #df = df.set_index(['algo'])
    #print df.sort(['algo','chunk'])
    #df.sort_index(inplace=True)
    fig_meta={#'title':'Average Processing time by different summarizers \n on avg. text length=%s'%math.ceil(df.iloc[0][df.columns[0]]),
              'title':'',
              'x-axis': 'Chunk Sizes',
              'y-axis': 'Processing Time',
              }
    if DRAW:
        if ttype == 'chunk':
            plot_3d(df.sort(['chunk']), "3d_algo_avg_ptime.pdf", fig_meta)
        elif ttype == 'algo':
            plot_3d_algo_perspective(df.sort(['chunk']), "tt3d_algo_avg_ptime.pdf", fig_meta)



def get_multi_chunk_algo_averages(chunk_sizes, summary_records):
    print 'chunk sizes', chunk_sizes
    print 'limit', summary_records
    ALGO_ENTRIES=7
    agg_pipeline = [
    		    {"$lookup": {"from": 'tweets', 'localField': 'parent_id', 'foreignField': '_id', 'as': 'tweet_docs'}},
		        {'$unwind': "$tweet_docs"},
		        {'$project': {'algos': True, 'ss': {'$size': '$tweet_docs.chunk'}, 'ca': {'$size': '$algos'}}},
		        {'$match': {'ss': {"$in": chunk_sizes}, 'ca':ALGO_ENTRIES}},
                #{'$limit': summary_records},
		        {"$unwind":'$algos'},
		        {"$group":{"_id":{"size":"$ss", "algo":"$algos.method"}, "avg_size":{"$avg": '$algos.size'}, "avg_time":{"$avg": "$algos.ptime"}}}
    		   ]

    res = db.processed.aggregate(agg_pipeline)
    #res = list(res)
    pprint (res)
    #resl = defaultdict(list)
    resl = []
    for doc in res:
        did = doc['_id']
        algo = did['algo']
        chunk = did['size']
        d = {'chunk': chunk, 'algo': algo, 'avg_size': doc['avg_size'], 'avg_time': doc['avg_time']}
        resl.append(d)
    #print resl
    df = pd.DataFrame(resl)
    #df = df.set_index(['chunk'])
    df.algo.replace(to_replace=dict(texrank='textrank'), inplace=True)
    #print df
    #df.sort_index(inplace=True)
    fig_meta={#'title':'Average Processing time by different summarizers \n on avg. text length=%s'%math.ceil(df.iloc[0][df.columns[0]]),
              'title':'',
              'x-axis': 'Tweet Chunk Sizes',
              'y-axis': 'Avg. Processing Time (sec)',
              }
    if DRAW:
        plot_combine_lines(df, "combined_algo_avg_ptime.pdf", fig_meta)

def get_algo_averages(chunk_size, summary_records):
    print("Average Algo Processing Times")
    ALGO_ENTRIES=7
    agg_pipeline = [
                 {"$lookup": {"from": 'tweets', 'localField': 'parent_id', 'foreignField': '_id', 'as': 'tweet_docs'}},
                 #{"$match": {'tweet_docs.size': chunk_size}},
                 {'$unwind': "$tweet_docs"},
                 {'$project': {'algos': True, 'ss': {'$size': '$tweet_docs.chunk'}, 'ca': {'$size': '$algos'}}},
                 {'$match': {'ss': chunk_size, 'ca':ALGO_ENTRIES}},
                 {"$limit": summary_records},
                 {"$project":{'algos.size': True, 'algos.method': True, 'algos.ptime': True}},
                 {"$unwind":'$algos'},
                 {"$group":{"_id":"$algos.method", "avg_size":{"$avg": '$algos.size'}, "avg_time":{"$avg": "$algos.ptime"}}},
                 {"$sort": {"_id":1}}
                 ]
    res = db.processed.aggregate(agg_pipeline)
    #pprint(list(res))
    df = pd.DataFrame.from_records(list(res))
    df._id.replace(to_replace=dict(texrank='textrank'), inplace=True)
    df = df.set_index(['_id'])
    legend = "avg_time"
    df.columns = ['Average Size', legend]
    print df
    ymax_limits = {10: 0.05, 15: 0.05, 20: 0.15, 25: 0.15, 30: 0.2, 35: 0.2}
    ymax = ymax_limits[chunk_size]
    fig_meta={#'title':'Average Processing time by different summarizers \n on avg. text length=%s'%math.ceil(df.iloc[0][df.columns[0]]),
              'title':'Chunk size= %s, Avg. Input text size=%s' % (chunk_size, math.ceil(df.iloc[0][df.columns[0]])),
              'x-axis': 'Summarization Algorithms',
              'y-axis': 'Avg. Processing Time (sec)',
              'ylimit': (0.0, ymax)}
    if DRAW:
	plot(df[[legend]], "avg_algo_ptime_%s.pdf" % chunk_size, fig_meta, False)

def get_avg_evaluation_score(chunk_size, summary_records):
    print("Average Evalation Score for chunk %s and records %s" % (chunk_size, summary_records))
    EVAL_ENTRIES=6
    agg_pipeline = [
                 #join collections, so that we can later filter doc having the given chunk size
                 {"$lookup": {"from": 'tweets', 'localField': 'parent_id', 'foreignField': '_id', 'as': 'tweet_docs'}},
                 #filter new documents with certain size
                 #{"$match": {'tweet_docs.size': chunk_size}},
                 {'$unwind': "$tweet_docs"},
                 {'$project': { 'sum_eval': True, 'ss': {'$size': '$tweet_docs.chunk'}, 'tca': {'$size': {'$ifNull':['$sum_eval',[]]} }}},
                 {'$match': {'ss': chunk_size, 'tca':EVAL_ENTRIES}},
                 #limiting records for next steps in this pipeline
                 {"$limit": summary_records},
                 #filter the fields we are interested in
                 {"$project":{ 'sum_eval.algo': True, 'sum_eval.rouge_1': True, 'sum_eval.rouge_2': True}},
                 #convert arrays into documents
                 {"$unwind":'$sum_eval'},
                 #perform group by aggregations
                 {"$group":{"_id":"$sum_eval.algo", "avg_rouge1":{"$avg": '$sum_eval.rouge_1'}, "avg_rouge2":{"$avg": "$sum_eval.rouge_2"}}},
                 {"$sort": {"_id":1}}
                 ]
    res = db.processed.aggregate(agg_pipeline)
    #print res
    #pprint(list(res))
    df = pd.DataFrame.from_records(list(res))
    df._id.replace(to_replace=dict(texrank='textrank'), inplace=True)
    df = df.set_index(['_id'])
    df.columns = ['Rouge 1', 'Rouge 2']
    print df
    fig_meta={#'title':'Evaluation Score Average by different summarizers for size=%s'%chunk_size,
              'title': '',
               'x-axis': 'Summarization Algorithm',
              'y-axis': 'Avg. Evaluation Score', 'ylimit': (0,1)}

    if DRAW:
    	plot(df, "avg_eval_score_%s.pdf" % chunk_size,fig_meta)

def plot(df, fname, fig_meta, LEGENDS=True):
    fig = plt.figure();
    ylim = fig_meta['ylimit']
    print 'Setting ylimit :', ylim
    #df.plot.bar();
    if not LEGENDS:
        df.plot(kind='bar', alpha=0.5, rot=0, legend=None, ylim=ylim)
    else:
        df.plot(kind='bar', alpha=0.5, rot=0, ylim=ylim)
    plt.suptitle(fig_meta['title'], fontsize=12)
    plt.xlabel(fig_meta['x-axis'], fontsize=14)
    plt.ylabel(fig_meta['y-axis'], fontsize=14)
    #plt.set_xticklabels(df.index,rotation=90)
    plt.savefig(fname, bbox_inches='tight')

def plot_combine_lines(df, fname, fig_meta):
    #http://matplotlib.org/api/markers_api.html#module-matplotlib.markers
    styles = {'kl': 'bs-', 'edmund':'ro-',
              'lsa': 'y^-','luhn':'*-', 'lexrank':'p-',
              'textrank':'x-', 'tfidf':'D-'}
    fig, ax = plt.subplots()
    labels = []
    for key, grp in df.groupby(['algo']):
        print 'ploting key ', key
        print grp
        ax = grp.sort(['chunk']).plot(xticks=[8, 10,15,20,25,30, 32], style=styles[key], ax=ax, kind='line', x='chunk', y='avg_time')
        labels.append(key)
    lines, _ = ax.get_legend_handles_labels()
    ax.grid(True)
    ax.legend(lines, labels, loc='best')
    #plt.suptitle(fig_meta['title'], fontsize=12)
    plt.xlabel(fig_meta['x-axis'], fontsize=14)
    plt.ylabel(fig_meta['y-axis'], fontsize=14)
    plt.savefig(fname, bbox_inches='tight')

def plot_3d(df, fname, fig_meta):
    #http://matplotlib.org/api/markers_api.html#module-matplotlib.markers
    styles = {'kl': 'bs-', 'edmund':'ro-',
              'lsa': 'y^-','luhn':'*-', 'lexrank':'p-',
              'textrank':'x-', 'tfidf':'D-'}

    markers = {'kl': '<', 'edmund':'o',
              'lsa': '^','luhn':'*', 'lexrank':'p',
              'textrank':'x', 'tfidf':'D'}
    ch_markers = {10: '<', 15:'o',
              20: '^', 25:'*', 30:'p',
              'textrank':'x', 'tfidf':'D'}

    colors = {'kl':'r', 'lsa':'g','textrank':'y',
              'luhn':'b', 'lexrank':'c','edmund':'m'}
    ch_colors = {10:'r', 15:'g',20:'y',
              25:'b', 30:'c'}

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    #ax.autoscale(False)
    ax.view_init(elev=19, azim=-63)
    labels=[]

    for key, grp in df.groupby(['algo']):
        print 'adding line ', key
        #data_pos = grp[['chunk', 'avg_time', 'rouge1']].as_matrix()
        #grp = grp.sort(['algo'])
        #print grp
        X = grp['chunk']
        #print X
        Y = grp['avg_time']
        Z = grp['rouge1']
        ax.plot(X, Y, Z.values,label=key, c=colors[key], marker=markers[key],)
        #ax.bar(X, Y, zs=Z.values, zdir='y', label=key, color=colors[key],)

        labels.append(key)

    lines, _ = ax.get_legend_handles_labels()
    ax.grid(True)
    ax.legend(lines, labels, loc='upper center')
    ax.set_xlabel(fig_meta['x-axis'], fontsize=10)
    ax.set_ylabel(fig_meta['y-axis'], fontsize=10)
    ax.set_zlabel('ROUGE-1', fontsize=12)
    plt.savefig(fname, bbox_inches='tight')
    plt.show()

def plot_3d_algo_perspective(df, fname, fig_meta):
    ch_markers = {10: '<', 15:'o',
              20: '^', 25:'*', 30:'p',
              'textrank':'x', 'tfidf':'D'}

    ch_colors = {10:'r', 15:'g',20:'y',
              25:'b', 30:'c'}

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    #ax.autoscale(False)
    ax.view_init(elev=19, azim=-63)
    #print df
    labels=[]
    def algo_to_number_map(algos):
        algos.algo.replace(to_replace=dict(edmund=101), inplace=True)
        algos.algo.replace(to_replace=dict(kl=102), inplace=True)
        algos.algo.replace(to_replace=dict(lexrank=103), inplace=True)
        algos.algo.replace(to_replace=dict(lsa=104), inplace=True)
        algos.algo.replace(to_replace=dict(luhn=105), inplace=True)
        algos.algo.replace(to_replace=dict(textrank=106), inplace=True)

    #ax.plot_surface(df[['algo','chunk']],df[['algo','avg_time']],df[['algo','rouge1']])
    for key, grp in df.groupby(['chunk']):
        print 'adding line ', key
        #print grp
        X = grp[['algo']]
        #print X
        #need to transform algo name to some int value
        algo_to_number_map(X)
        Y = grp['avg_time']
        Z = grp['rouge1']
        print X.sort()
        #print Y
        #print Z
        #ax.plot(X, Y, Z.values,label=key, c=ch_colors[key], marker=ch_markers[key],)
        ax.bar(X, Y, zs=Z, zdir='x', color=ch_colors[key])
        #ax.plot_wireframe(X, Y, Z.values, color=ch_colors[key])

        labels.append(key)

    lines, _ = ax.get_legend_handles_labels()
    ax.grid(True)
    ax.legend(lines, labels, loc='upper center')

    ax.set_xlabel(fig_meta['x-axis'], fontsize=10)
    ax.set_ylabel(fig_meta['y-axis'], fontsize=10)
    ax.set_zlabel('ROUGE-1', fontsize=12)
    plt.savefig(fname, bbox_inches='tight')
    plt.show()

def test_3d():
    from mpl_toolkits.mplot3d.axes3d import Axes3D
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(subplot_kw={'projection': '3d'})
    datasets = [{"x":[1,2,3], "y":[1,4,9], "z":[0,0,0], "colour": "red"} for _ in range(6)]
    print datasets
    for dataset in datasets:
        print dataset['x']
        ax.plot(dataset["x"], dataset["y"], dataset["z"], color=dataset["colour"])
    plt.show()

def test2_3d():
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    pos1 = np.array([[0,0,0],[2,2,2]])
    pos2 = np.array([[1,1,1],[3,3,3]])

    for point_pairs in zip(pos1, pos2):
        xs, ys, zs = zip(*point_pairs)
        print 'xs',xs
        print 'ys',ys
        print 'zs',zs
        ax.plot(xs, ys, zs)
    plt.show()

def init():
    global mongo, db, MONGO_URLS
    mongo = MongoClient(MONGO_URLS)
    db = mongo.twitter

def usage():
    print('Usage: %s i|m|3d <max summary records> <chunk size (int)>' % (__file__))
    print('i: individual chunk graph')
    print('m: multi-lines algo graph')
    print('<max summary records>: (int) number of records to consider')
    print('<chunk size>: (int) to be used along with "i" option')

if __name__ == '__main__':
    if len(sys.argv) < 2:
        usage()
        sys.exit(1)

    opt = sys.argv[1]
    init()
    if opt == 'i':
        SUMMARY_RECORDS= sys.argv[2]
        CHUNK_SIZE = sys.argv[3]
        #test_pipeline(int(CHUNK_SIZE))
        get_algo_averages(int(CHUNK_SIZE), int(SUMMARY_RECORDS))
        get_avg_evaluation_score(int(CHUNK_SIZE), int(SUMMARY_RECORDS))
    elif opt == 'm':
        SUMMARY_RECORDS= sys.argv[2]
        get_multi_chunk_algo_averages([10,15,20,25,30], int(SUMMARY_RECORDS))
    elif opt == '3d':
        SUMMARY_RECORDS= sys.argv[2]
        get_multi_chunk_algo_3d([10,15,20,25,30], int(SUMMARY_RECORDS), 'chunk')
        #test_3d()
        #test2_3d()
        #get_multi_chunk_algo_3d([10,15,20,25,30], int(SUMMARY_RECORDS), 'algo')

    else:
        print ('Option not found')
        usage()
