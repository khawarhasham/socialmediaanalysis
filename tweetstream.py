from tweepy.streaming import StreamListener
import tweepy
from tweet_queue import TweetQueue
from pprint import pprint
from urlparse import urlparse
from collections import Counter
import json,time,sys
import signal #to handle signals, especially keyboard interrupt
import re
import sconfig

#stats counter
stats = Counter()
stats['stream_start'] = time.time()

class SListener(StreamListener):
    def __init__(self, api=None, fprefix="streamer"):
        self.api = api
        self.counter = 0
        self.fprefix = fprefix
        self.news_agencies=sconfig.news_agencies
        self.tweet_queue = TweetQueue()

        #self.producer = None
        self.producer = self.tweet_queue.get_sync_producer('tweet')
        self.articleProducer = self.tweet_queue.get_sync_producer('tweet_article')
        print self.producer

    #old method parsing the tweet text. Use tweet's entities object
    def get_article_from_tweet(self, text):
        pattern = r'(https?://[^\s]+)'
        links = re.findall(pattern, text)
        return links

    def get_links_from_tweet(self, tweet):
        entities = tweet['entities']
        #print tweet['id']
        if self.filter_media(entities):
            return self.get_emd_article_tweet(tweet)

        #pprint (entities)
        #print extended_entities['type']
        return None

    def get_emd_article_tweet(self, tweet):
        links = {}
        for url in tweet['entities']['urls']:
            if url['expanded_url'] is None:
                continue
            expanded_url = url['expanded_url']
            parsed_uri = urlparse(expanded_url)
            #print parsed_uri.netloc, expanded_url
            links[parsed_uri.netloc] = expanded_url

        found = False
        for news_source, news_link in links.items():
            for n in self.news_agencies:
                if ( news_source.find(n)>=0):
                    found = True
                    break
        if found:
            return links.values()
        else:
            #print("No links found")
            return None

    def filter_language(self, tweet):
        if 'lang' in tweet and tweet['lang'].lower() == 'en':
            return True
        return False

    #filter non-retweeted tweets
    def filter_retweet(self, tweet):
        if 'retweeted_status' not in tweet:
            return True
        return False

    #filter only links having textual info. not any photo, video etc.
    #TODO: WHAT IF a news link is shared in text and a photo is also embedded in same tweet?
    #in this case we will have media object as well as urls.
    #Urls will contain the news link and media will have the other object.
    #Think about it later
    def filter_media(self, tweet_entities):
        if 'urls' in tweet_entities and 'media' not in tweet_entities:
            return True
        return False

    def publish_article_tweet(self, tweet_text):
        print 'publishing articles'
        if self.articleProducer:
            self.articleProducer.produce(tweet_text)

    def publish_tweet_text(self, data):
        print 'publishing tweet'
        if self.producer:
            self.producer.produce(data)

    def remove_link_and_mentions(self, tweet_txt):
        #remove URLs and @mentions from the text
        #tweet_txt = re.sub(r"(?:\@|https?\://)\S+", "", tweet_txt)
        return re.sub(r"(?:\@|https?\://)\S+", "", tweet_txt)

    def on_data(self, data):
        #print (type(data))
        tweet = json.loads(data)
        stats['tweets'] += 1

        #pprint (tweet)
        #print type(data)
        #filter tweets with language english and also not a retweet to avoid processing same links again
        #print tweet['text']
        if self.filter_language(tweet) and self.filter_retweet(tweet):
            print tweet['text'], "==>", tweet['lang']
            stats['tweet_english'] += 1
            links = self.get_links_from_tweet(tweet)
            if links:
                #self.publish_article_tweet(tweet['text'].encode('ascii', 'ignore'))
                if sconfig.TWEET_ARTICLE:
                    self.publish_article_tweet(data)
                    #print links
                stats['tweet_links'] += 1
            else:
                #print 'no prefix domain links'
                stats['tweet_nolinks'] += 1

            #also put tweet text after removing links from it
            if sconfig.TWEET_TEXT:
                #tweet_text = self.remove_link_and_mentions(tweet['text'])
                links = self.get_article_from_tweet(tweet['text'])
                #if len(tweet_text) > 6:
                if (links == None or len(links) == 0) and len(tweet['text'].split()) > 4:
                    #print json.dumps(tweet).decode('unicode-escape').encode('utf8')
                    self.publish_tweet_text(data.encode('UTF-8'))
                    stats['tweet_text'] += 1

        else:
            #print 'tweet language not english.'
            #print tweet
            stats['tweet_noenglish'] += 1

        #self.tweet_queue.produce_msg('tweet', data)
        return True

    def on_status(self, status):
        for hashtag in status.entries['hashtags']:
            print(hashtag['text'])

        return True

    def on_error(self, status):
        print (status)

    def on_timeout(self):
        print ('Snoozing ZzzzZzzz')

def start_stream(keywords):
    #access credentials for twitter's firehose stream
    consumer_key="FnHTmhiDNMYLQJog8LC5Gg"
    consumer_sec="EHfcea87qdzWnZITljLhzxo9SHn4kVmRHsvmcpXMAGA"
    access_token="94594745-YSW30W9NsuzGM4rQXvpZNkrUgwRxbz2LlYqcCvenc"
    access_sec="FEggZpaLY6YkBEWCjRIwxUcmUnJOstWry7HSYBt5E"

    auth = tweepy.OAuthHandler(consumer_key, consumer_sec)
    auth.set_access_token(access_token, access_sec)

    sl = SListener()
    #connecting to Twitter's firehose stream to access all public statuses
    stream = tweepy.Stream(auth, sl)
    #change from CWC2015 to someothing else
    #stream.filter(track=keywords.split(","))
    try:
        stream.filter(track=keywords.split(","))#, languages="en") #,sep keywords means OR search, space sep keywords mean AND search
    except:
        print sys.exc_info()
        print_stats()


def simple_auth():
    pass

def handler(signum, frame):
    print ('Singal handler called with Signal {} and frame {}'.format(signum, frame))
    print_stats()
    sys.exit(1)

def print_stats():
    stats['stream_finished'] = time.time()
    stats['stream_total_time'] = stats['stream_finished'] - stats['stream_start']
    print ('='*25)
    print ('TweetStream Stats')
    print stats
    print ('='*25)

def usage():
    print ('usage: tweetstream.py [stream|other] <comma-separated keywords>')

if __name__=='__main__':
    signal.signal(signal.SIGINT, handler)
    if len(sys.argv) < 2:
        usage()
        sys.exit(1)

    if sys.argv[1]=='stream':
        keywords = sys.argv[2]
        print ('keywords: ', keywords)
        start_stream(keywords)
    elif sys.argv[1]=='other':
        print ('do something else here')


#https://pypi.python.org/pypi/pyrouge/0.1.3
